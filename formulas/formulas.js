class Formulas {
    constructor() {

    }

    getRunningAverage(data, field) {
        const header     = data[0];
        let   fieldIndex = header.indexOf(field);

        const runningAverage = [];
        let   runningSum     = 0;
        for (let i = 1; i < data.length; i++) { // Start at first non-header row
            const n = Number(data[i][fieldIndex]);

            // Don't sum up values that aren't a number
            if (!Number.isNaN(n)) {
                runningSum += n;
            }

            runningAverage.push(runningSum / (i+1));
        }

        return runningAverage;
    }
}

module.exports = Formulas;