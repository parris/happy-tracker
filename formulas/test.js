const Formulas = require('./formulas');

const goodData = [
    ['date',        'wasHappy', 'uselessField'],
    ['20180101',    '1',        '0'],
    ['20180102',    '0',        '1'],
    ['20180103',    '1',        '2'],
    ['20180104',    '0',        '3'],
    ['20180105',    '1',        '4'],
    ['20180106',    '1',        '5'],
    ['20180107',    '1',        '6'],
    ['20180108',    '1',        '7'],
    ['20180109',    '0',        '8'],
    ['20180110',    '0',        '9'],
];

/**
 * Tests the running averages endpoint
 * @returns {Promise<void>}
 */
const testRunningAverages = async (data, field) => {
    try {
        const formula = new Formulas();
        return await formula.getRunningAverage(data, field);
    } catch (err) {
        console.log(err);
        throw err;
    }
};

const testPositiveCase = testRunningAverages(goodData, 'wasHappy');

Promise.all([
    testPositiveCase,
])
.then(status => {
    let passed = true;
    // Validate the first few records of the runningAverage return array
    if (!Array.isArray(status[0])) {
        console.log('runningAverage returned value is not an array', status[0]);
        passed = false;
    } else {
        if (status[0][0] !== 1) {
            console.log('runningAverage first data point should be 1', status[0]);
        }
        if (status[0][1] !== 0.5) {
            console.log('runningAverage second data point should be 0.5', status[0]);
        }
        if (status[0][3] !== 0.5) {
            console.log('runningAverage fourth data point should be 0.5', status[0]);
        }
    }

    if (passed) {
        console.log("All tests passed.");
        process.exit(0);
    } else {
        console.log("Some tests failed.");
        process.exit(1);
    }
});