const handler = require('./handler');

const testParsePostBody = () => {
    const postBodyString = 'email=test%40gmail.com&activities%5B%5D=work+conflict&activities%5B%5D=code&activities%5B%5D=eat+junk+food&activities%5B%5D=conversation+with+kerri&activities%5B%5D=hang+with+p&activities%5B%5D=sleep+well&activities%5B%5D=lift+weights&activities%5B%5D=run&activities%5B%5D='
    return handler.parsePostBody(postBodyString);
};

Promise.all([
    testParsePostBody(),
]).then(results => {
    let passed = true;

    // Test email gets parsed correctly
    if (results[0]['email'] !== 'test@gmail.com') {
        passed = false;
        console.log("testParsePostBody failed... Could not parse email");
    }

    // Test activities get stored as an array, with the correct length
    if (results[0]['activities'].length !== 9) {
        passed = false;
        console.log("testParsePostBody failed... Could not parse activities");
    }

    if (passed) {
        console.log("Success.");
        process.exit(0);
    } else {
        console.log("There were failures...");
        process.exit(1);
    }
});