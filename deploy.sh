#!/bin/bash

# Deploy lambdas if an "s3" flag isn't provided
if [ "$1" != "--s3" ]; then
    serverless deploy --aws-profile parris-home
fi

aws --profile parris-home s3 cp ./website s3://www.happytracker.com --acl public-read --recursive