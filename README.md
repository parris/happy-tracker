# Daily Tracker

[Currently hosted via S3 static website...](http://www.happytracker.com.s3-website-us-east-1.amazonaws.com/)

Receive an email every day around bedtime asking how your day was. (I know you check your email in bed).

After responding positively or negatively, you'll be asked about the things you've decided to track.

Over time you'll be able to correlate the things you are tracking with your happiness.

# Examples of things to track

* I exercised
* I ate breakfast
* I wrote code
* I used social media
* I ate gluten

