const TrackedActivities = require('./tracked-activities');
const Auth              = require('../auth/auth');

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const S3 = new AWS.S3();

const auth = new Auth('this is a test');
const trackedActivities = new TrackedActivities(auth, S3);

// Don't configure auth for "check" endpoints
const authNotSetup = new Auth();
const trackedActivitiesNoAuth = new TrackedActivities(authNotSetup, S3);

const testSave = async (data, activities) => {
    try {
        return await trackedActivities.update(data, activities);
    } catch (err) {
        console.log(err);
        return err;
    }
};

const testCreate = async (email, activities) => {
    try {
        return await trackedActivitiesNoAuth.create(email, activities);
    } catch (err) {
        return err;
    }
};

const testGet = async (data) => {
    try {
        return await trackedActivities.get(data);
    } catch (err) {
        console.log(err);
        return err;
    }
};

const testCheck = async (email) => {
    try {
        return await trackedActivitiesNoAuth.checkIfAvailable(email);
    } catch (err) {
        console.log(err);
        return err;
    }
};

const encryptedTestRequest = auth.generateEncryptedRequest({email:'test@gmail.com'});
const testActivities = ['never', 'gonna', 'give', 'you', 'up', ''];

const t0 = testSave(encryptedTestRequest, testActivities); // Should update test@gmail.com
const t1 = testGet(auth.generateEncryptedRequest({email:'test@gmail.com'})); // Should return an array of activities
const t2 = testGet(auth.generateEncryptedRequest({email: 'parris.oops@gmail.com'})); // An invalid email should return false
const t3 = testCheck('test@gmail.com'); // Should return true
const t4 = testCheck('parris.oops@gmail.com'); // Should return false
const t5 = testCreate('test@gmail.com', testActivities); // Should throw an error

Promise
    .all([t0, t1, t2, t3, t4, t5])
    .then(status => {
        let passed = true;

        // Test that testSave returns an ETag on success
        if (!status[0]['ETag']) {
            console.log("t0 failed.");
            passed = false;
        }

        // Test that testGet returns an array on success
        if (!Array.isArray(status[1])) {
            console.log('t1 failed... not an array');
            passed = false;
        } else {
            // Test that testGet returns the correct data
            for (let i in testActivities) {
                if (status[1][i] !== testActivities[i] && testActivities[i] !== '') {
                    console.log('t1 failed... wrong results');
                    passed = false;
                }
            }
        }

        // Test that testGet returns false on a bad email
        if (status[2]) {
            console.log("t2 failed.");
            passed = false;
        }

        // Test that check() correctly reports an existing user
        if (status[3]) {
            console.log("t3 fails to report user already exists");
            passed = false;
        }

        // Test that check() correctly reports an non-existing user
        if (!status[4]) {
            console.log("t4 fails to report user does not already exists");
            passed = false;
        }

        // Test that create() throws an error on duplicate account
        if (!status[5] instanceof Error) {
            console.log("t5 fails to report user already exists");
            passed = false;
        }

        if (passed) {
            console.log("All tests passed.");
            process.exit(0);
        } else {
            console.log("Some tests failed.");
            process.exit(1);
        }
});