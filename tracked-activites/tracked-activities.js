class TrackedActivities {
    constructor(auth, S3) {
        this.decrypt = auth.decryptEncryptedRequest.bind(auth);
        this.s3      = S3;
    }

    /**
     * Checks whether an email is already associated with a tracked activities.
     * @param {string} email
     * @returns {Promise}
     *   Returns "true" if the checked email is available, and false otherwise.
     */
    async checkIfAvailable(email) {
        const key = email + '/tracked-activities.json';

        try {
            await this.s3.getObject({
                Bucket: "happytracker",
                Key:    key,
            }).promise();

            return false;
        } catch (err) {
            if (err['code'] === "NoSuchKey") {
                return true;
            }

            throw err;
        }
    }

    /**
     * Creates a new user's tracked activities to S3, errors if user already exists
     * @param email
     * @param activities
     * @returns {Promise}
     */
    async create(email, activities) {
        const key  = email + '/tracked-activities.json';
        const body = [];

        if (!this.checkIfAvailable(email)) {
            throw new Error("Cannot create tracked activities: account already exists.")
        }

        // Strip out empty activities
        for (let activity of activities) {
            if (!!activity) {
                body.push(activity);
            }
        }

        console.log(`Key: ${key}\nBody:${body}`);

        const putSuccess = await this.s3.putObject({
            Bucket: "happytracker",
            Key:    key,
            Body:   JSON.stringify(body),
        }).promise();

        return await putSuccess;
    }

    /**
     * Gets a user's list of tracked activities
     * @param data
     * @returns {Promise<*>}
     */
    async get(data) {
        const decrypted = this.decrypt(data);
        const email     = decrypted['email'];
        const key       = email + '/tracked-activities.json';

        try {
            const putSuccess = await this.s3.getObject({
                Bucket: "happytracker",
                Key:    key,
            }).promise();

            return JSON.parse(putSuccess.Body.toString('utf-8'));
        } catch (err) {
            if (err['code'] === "NoSuchKey") {
                return false;
            }

            throw err;
        }
    }

    /**
     * Updates an existing user's tracked activities to S3
     * @param {string} data
     * @param {array} activities
     * @returns {Promise}
     */
    async update(data, activities) {
        const decrypted = this.decrypt(data);
        const email     = decrypted['email'];
        const key       = email + '/tracked-activities.json';

        const body = [];

        // Strip out empty activities
        for (let activity of activities) {
            if (!!activity) {
                body.push(activity);
            }
        }

        const putSuccess = await this.s3.putObject({
            Bucket: "happytracker",
            Key:    key,
            Body:   JSON.stringify(body),
        }).promise();

        return await putSuccess;
    }
}

module.exports = TrackedActivities;