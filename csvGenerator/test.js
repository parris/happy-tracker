const CsvGenerator      = require('./csvGenerator');
const DailyData         = require('../saveDailyData/saveDailyData');
const TrackedActivities = require('../tracked-activites/tracked-activities');
const Auth              = require('../auth/auth');
const Formulas          = require('../formulas/formulas');

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

const S3                = new AWS.S3();
const auth              = new Auth('secret password');
const dailyData         = new DailyData(auth, S3);
const trackedActivities = new TrackedActivities(auth, S3);
const formulas          = new Formulas();

/**
 * Tests the generateCsvUrl endpoint
 * @returns {Promise<void>}
 */
const testGenerateCsvUrl = async (email) => {
    try {
        const csvGenerator   = new CsvGenerator(S3, formulas);
        const encryptedEmail = auth.generateEncryptedRequest({email});
        const data           = await dailyData.getAll(encryptedEmail);

        return await csvGenerator.generateCsvUrl('allData', data);
    } catch (err) {
        console.log(err);
        throw err;
    }
};

/**
 * Tests the generateCorrelationData endpoint
 * @returns {Promise<void>}
 */
const testGenerateCorrelationData = async (email) => {
    try {
        const csvGenerator    = new CsvGenerator(S3, formulas);
        const encryptedEmail  = auth.generateEncryptedRequest({email});
        const data            = dailyData.getAll(encryptedEmail);
        const activities      = trackedActivities.get(encryptedEmail);

        const correlationData = await csvGenerator.generateCorrelationData(await data, await activities);
        await csvGenerator.generateCsvUrl('correlation', correlationData); // For manual testing

        return correlationData;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

const testCsvData         = testGenerateCsvUrl('parris.varney@gmail.com');
const testCorrelationData = testGenerateCorrelationData('parris.varney@gmail.com');

Promise
    .all([
        testCsvData,
        testCorrelationData,
    ])
    .then(status => {
        let passed = true;
        // Test URL is generated
        if (!status[0].match(/Expires/)) {
            console.log('t0 failed... did not generate a URL with an expiration', status[0]);
            passed = false;
        }

        if (status[1].length <= 1) {
            console.log('t1 failed... expected more than one record to be returned', status[1]);
            passed = false;
        }

        if (passed) {
            console.log("All tests passed.");
            process.exit(0);
        } else {
            console.log("Some tests failed.");
            process.exit(1);
        }
    });