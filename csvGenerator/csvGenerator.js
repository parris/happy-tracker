class CsvGenerator {
    /**
     * @param {S3} S3
     * @param {Formulas} Formulas
     */
    constructor(S3, Formulas) {
        this.s3                 = S3;
        this.correlationFormula = Formulas.getRunningAverage;
    }

    async generateCsvUrl(name, data) {
        // Create the CSV
        let csv = "";
        for (let d of data) {
            csv += d.join(',') + "\n";
        }

        // Push the CSV to S3
        const fileName = name + '-' + (new Date().toISOString().slice(0, 10)) + '.csv';
        await this.s3.putObject({Key: fileName, Bucket: 'happytracker/reports', Body: csv}).promise();

        // Create a URL for accessing the file
        const url = this.s3.getSignedUrl('getObject', {
            Bucket:  'happytracker/reports',
            Key:     fileName,
            Expires: 600,
        });

        return url;
    }

    async generateCorrelationData(data, activities) {
        const correlationData = {
            happy: this.correlationFormula(data, 'wasHappy'),
        };

        for (let a of activities) {
            correlationData[a] = (this.correlationFormula(data, a));
        }

        // Build header row
        const csvData = [Object.keys(correlationData)];

        // Build each row
        for (let [activity, data] of Object.entries(correlationData)) {
            const headerIndex = csvData[0].indexOf(activity);
            let   dataRowNum  = 1;
            for (let d of data) {
                if (!csvData[dataRowNum]) {
                    csvData.push([]);
                }

                csvData[dataRowNum][headerIndex] = d;
                dataRowNum++
            }
        }

        return csvData;
    }


}

module.exports = CsvGenerator;