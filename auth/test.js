const Auth = require('./auth');

const secret  = 'Never gonna give you up';
const testMsg = {
    email:    'parris.varney@gmail.com',
    wasHappy: true,
};

function testEncrypt() {
    try {
        const auth = new Auth(secret);
        return auth.generateEncryptedRequest(testMsg);
    } catch (err) {
        console.log(err);
        throw err;
    }
}

function testDecrypt(msg) {
    try {
        const auth = new Auth(secret);
        return auth.decryptEncryptedRequest(msg);
    } catch (err) {
        return err.message;
    }
}

function testBadSecret(msg) {
    try {
        const auth = new Auth('Let you down');
        return auth.decryptEncryptedRequest(msg);
    } catch (err) {
        return err.message;
    }
}

const encryptedRequest = testEncrypt();
const decryptedMessage = testDecrypt(encryptedRequest);
const badMessage       = testDecrypt('BAD'); // Hex string representing 2989
const badSecret        = testBadSecret(encryptedRequest);

let passed = true;

if (decryptedMessage['email'] !== testMsg['email']) {
    passed = false;
    console.log("Emailed failed to decrypt.");
}

if (decryptedMessage['wasHappy'] !== testMsg['wasHappy']) {
    passed = false;
    console.log("wasHappy failed to decrypt.");
}

if (badMessage !== 'Bad input string') {
    passed = false;
    console.log("Failed to report an incorrectly encrypted message.");
}

if (!badSecret.match(/bad decrypt$/)) {
    passed = false;
    console.log("Failed to report a bad secret key.");
}

if (passed) {
    console.log('Success.');
    process.exit(0);
} else {
    console.log('Some tests failed.');
    process.exit(1);
}