class Auth {
    constructor(secret) {
        this.crypto    = require('crypto');
        this.secret    = secret;
        this.algorithm = 'aes192';
    }

    /**
     * Encrypts a JSON object into a hex string
     * @param body
     */
    generateEncryptedRequest(body) {
        // These parameters will override anything in the body parameter
        const requiredBodyParameters = {
            "exp": this.getTokenExp().getTime(),
        };

        // Override body parameter with required parameters
        body = Object.assign(body, requiredBodyParameters);

        // Create and return encrypted message
        const cipher = this.crypto.createCipher(this.algorithm, this.secret);
        return cipher.update(JSON.stringify(body), 'utf8', 'hex') + cipher.final('hex');
    }

    /**
     * Returns the decrypted request
     * @param {string} body
     * @returns {object}
     */
    decryptEncryptedRequest(body) {
        const decipher = this.crypto.createDecipher(this.algorithm, this.secret);
        const msg = decipher.update(body, 'hex', 'utf8') + decipher.final('utf8');

        const json = JSON.parse(msg);

        const now = (new Date()).getTime();
        if (now > json['exp']) {
            throw new Error('Request expired.');
        }  else {
            return json;
        }
    }

    /**
     * Returns tomorrow at 11:59:59PM
     * @returns {Date}
     */
    getTokenExp() {
        const tomorrow = new Date();
        tomorrow.setDate((new Date()).getDate()+1);
        tomorrow.setHours(23, 59, 59);

        return tomorrow;
    }
}

module.exports = Auth;

