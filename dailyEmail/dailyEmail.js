class DailyEmail {
    /**
     * @param {Auth} auth
     * @param {SES} ses
     * @param {DynamoDB} dynamo
     */
    constructor(auth, ses, dynamo) {
        this.dynamo  = dynamo;
        this.ses     = ses;
        this.encrypt = auth.generateEncryptedRequest.bind(auth);
    }

    /**
     * Retrieves a list of users from Dynamo, then calls this.send() on each
     * @param {Date} date
     * @returns {Promise<void>}
     */
    async sendAll(date) {
        const params = {
            TableName: 'tracker-users',
        };

        const emails  = await this.dynamo.scan(params).promise();
        const results = [];

        for (let email of emails.Items) {
            results.push(this.send(email.email['S'], date));
        }

        return Promise.all(results);
    }

    /**
     * Sends the daily email to a single email address
     * @param toAddress
     * @param date
     * @returns {Promise<PromiseResult<D, E>>}
     */
    async send(toAddress, date) {
        const yesParam = this.encrypt({email: toAddress, wasHappy: 'yes'});
        const noParam  = this.encrypt({email: toAddress, wasHappy: 'no'});

        const year     = date.getFullYear();
        const month    = ("0" + (date.getMonth() + 1)).slice(-2); // getMonth() returns 0 indexed
        const day      = ("0" + date.getDate()).slice(-2); // getDate does not return a leading 0
        const urlDate  =  `${year}${month}${day}`;

        const params = {
            Destination: {
                ToAddresses: [toAddress]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `
<h2>Did you have a good day?</h2>
<p><a href="http://www.happytracker.com.s3-website-us-east-1.amazonaws.com/enter-daily-data.html?h=${yesParam}&d=${urlDate}">Yes, my day was pretty good</a></p>
<p><a href="http://www.happytracker.com.s3-website-us-east-1.amazonaws.com/enter-daily-data.html?h=${noParam}&d=${urlDate}">No, it wasn't great</a></p>`,
                    },
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: 'How was your ' + date.toDateString(),
                }
            },
            Source: 'parris.varney@gmail.com',
        };

        return await this.ses.sendEmail(params).promise();
    }
}

module.exports = DailyEmail;