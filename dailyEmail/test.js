const DailyEmail = require('./dailyEmail');
const Auth       = require('../auth/auth');


const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

const SES      = new AWS.SES({apiVersion: '2010-12-01'});
const DynamoDb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const date     = new Date('2018-01-01');

// Mock out the SES object - we don't need to actually send emails during tests
const MockSES = {
    returnVal: null,

    sendEmail: function(params) {
        this.returnVal = params;

        return this;
    },

    promise: function() {
        return Promise.resolve(this.returnVal);
    },
};

/**
 * Tests the sendAll endpoint
 * @returns {Promise<void>}
 */
const testSendAll = async () => {
    try {
        const dailyEmail = new DailyEmail(Auth, MockSES, DynamoDb);
        return await dailyEmail.sendAll(date);
    } catch (err) {
        console.log(err);
        throw err;
    }
};

const t0 = testSendAll();

Promise
    .all([t0])
    .then(status => {
        let passed = true;
        // Test sendAll() emailed parris.varney@gmail.com
        if (!Array.isArray(status[0])) {
            console.log('t0 failed... returned value is not an array');
            passed = false;
        } else {
            let foundParris = false;
            for (let i in status[0]) {
                if (status[0][i]["Source"] === 'parris.varney@gmail.com' ) {
                    foundParris = true;
                }
            }

            if (!foundParris) {
                console.log('t0 failed... could not find parris.varney@gmail.com in Dynamo');
                passed = false;
            }
        }

        if (passed) {
            console.log("All tests passed.");
            process.exit(0);
        } else {
            console.log("Some tests failed.");
            process.exit(1);
        }
    });