const qs  = require('querystring');
const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

const Auth              = require('./auth/auth');
const DailyEmail        = require('./dailyEmail/dailyEmail');
const SaveDailyData     = require('./saveDailyData/saveDailyData');
const TrackedActivities = require('./tracked-activites/tracked-activities');

/**
 * Sends the daily "how was your day" email
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<{obj}>}
 */
module.exports.sendDailyEmail = async (event, context, callback) => {
    try {
        const auth       = new Auth(process.env.ENCRYPTION_PASS);
        const SES        = new AWS.SES({apiVersion: '2010-12-01'});
        const DynamoDb   = new AWS.DynamoDB({apiVersion: '2012-08-10'});
        const dailyEmail = new DailyEmail(auth, SES, DynamoDb);

        const date    = new Date();
        const success = await dailyEmail.sendAll(date);

        return respondJSON(200, success);
    } catch (err) {
        console.log(err);
        return respondJSON(400, err);
    }
};

/**
 * Sends the daily "how was your day" email to parris.varney@gmail.com
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<{obj}>}
 */
module.exports.sendTestEmail = async (event, context, callback) => {
    try {
        const auth       = new Auth(process.env.ENCRYPTION_PASS);
        const SES        = new AWS.SES({apiVersion: '2010-12-01'});
        const DynamoDb   = new AWS.DynamoDB({apiVersion: '2012-08-10'});
        const dailyEmail = new DailyEmail(auth, SES, DynamoDb);

        const date    = new Date();
        const success = await dailyEmail.send('parris.varney@gmail.com', date);

        return respondJSON(200, success);
    } catch (err) {
        console.log(err);
        return respondJSON(400, err);
    }
};

/**
 * Saves the user input from the "enter-daily-data" form
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<{obj}>}
 */
module.exports.saveDailyData = async (event, context, callback) => {
    try {
        const auth          = new Auth(process.env.ENCRYPTION_PASS);
        const S3            = new AWS.S3();
        const saveDailyData = new SaveDailyData(auth, S3);

        const data    = this.parsePostBody(event.body);
        console.log(data, event.body);
        const success = await saveDailyData.save(data);

        console.log("Successful save.", data, success);
        return respondJSON(200, data);
    } catch (err) {
        console.log(err);
        return respondJSON(400, err);
    }
};

/**
 * Returns true if a an email address is already associated with tracked activities, false otherwise
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<{obj}>}
 */
module.exports.checkTrackedActivities = async (event, context, callback) => {
    try {
        const auth = new Auth(); // Don't configure auth, we don't want it available for this endpoint
        const S3   = new AWS.S3();
        const trackedActivities = new TrackedActivities(auth, S3);

        const email = event['queryStringParameters']['e'];

        const success = await trackedActivities.checkIfAvailable(email);
        const statusCode = success ? 200 : 401;

        return respondJSON(statusCode, success);
    } catch (err) {
        console.log(err);
        return respondJSON(400, err.toString());
    }
};

/**
 * Gets the activities tracked by the user
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<{obj}>}
 */
module.exports.getTrackedActivities = async (event, context, callback) => {
    try {
        const auth = new Auth(process.env.ENCRYPTION_PASS);
        const S3   = new AWS.S3();
        const trackedActivities = new TrackedActivities(auth, S3);

        const data  = event['queryStringParameters']['d'];

        return respondJSON(200, await trackedActivities.get(data));
    } catch (err) {
        console.log(err);
        return respondJSON(400, err.toString());
    }
};

/**
 * Saves activities tracked by a new user
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<{obj}>}
 */
module.exports.createTrackedActivities = async (event, context, callback) => {
    try {
        const auth = new Auth(); // Don't configure auth
        const S3   = new AWS.S3();
        const trackedActivities = new TrackedActivities(auth, S3);

        const data       = this.parsePostBody(event.body);
        const email      = data['email'];
        const activities = data['activities'];

        return respondJSON(200, await trackedActivities.create(email, activities));
    } catch (err) {
        console.log(err);
        return respondJSON(400, err.toString());
    }
};

/**
 * Saves activities tracked by an existing user
 * @param event
 * @param context
 * @param callback
 * @returns {Promise<{obj}>}
 */
module.exports.updateTrackedActivities = async (event, context, callback) => {
    try {
        const auth = new Auth(process.env.ENCRYPTION_PASS);
        const S3   = new AWS.S3();
        const trackedActivities = new TrackedActivities(auth, S3);

        const postBody   = this.parsePostBody(event.body);
        const data       = postBody['data'];
        const activities = postBody['activities'];

        return respondJSON(200, await trackedActivities.update(data, activities));
    } catch (err) {
        console.log(err);
        return respondJSON(400, err.toString());
    }
};

/**
 * Returns a render-able response
 * @param {int} code
 *  An HTTP status code
 * @param {string} html
 *  The response HTML
 * @returns {Promise<{obj}>}
 */
async function respondHTML(code, html) {
    return await {
        statusCode: code,
        headers: {
            'Content-Type': 'text/html',
        },
        body: html,
    };
}

/**
 * Returns a JSON response
 * @param {int} code
 *  An HTTP status code
 * @param {object} json
 *  The response json
 * @returns {Promise<{}>}
 */
async function respondJSON(code, json) {
    return await {
        statusCode: code,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(json),
    };
}

/**
 * Parses a form encoded post body query string.
 *
 * 1. Fields with empty values are removed
 * 2. Arrays with a name ending in [] will have the [] removed from the name
 * 3. For arrays not ending in [], the last element will be used as the value
 *
 * @param {string} body
 */
module.exports.parsePostBody = (body) => {
    const parsedResult = {};

    const parsedBody = qs.parse(body);

    for (let [k, v] of Object.entries(parsedBody)) {
        // Parse arrays (arrays have [] in the name)
        if (k.match(/\[\]$/)) {
            // Strip the [] from the form names
            k = k.replace(/\[\]$/, '');
        }

        // Checkboxes are will be sent as ["off", "on"] if they're checked.  Parse these to just "on".  They are
        // differentiated from "real" arrays because they won't have "[]" at the end of their name.
        else if (Array.isArray(v)) {
            v = v[v.length-1];
        }

        // Skip empty form items
        if (!!v) {
            parsedResult[k] = v;
        }
    }

    return parsedResult;
};