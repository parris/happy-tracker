const SaveDailyData = require('./saveDailyData');
const Auth          = require('../auth/auth');

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const S3 = new AWS.S3();

const auth = new Auth('Never gonna give you up');
const body = {
    date:       '20180102',
    wasHappy:   auth.generateEncryptedRequest({email: 'parris.test@gmail.com', wasHappy: 'yes'}),
    beer:       'on',
};

/**
 * Tests the save endpoint
 * @returns {Promise<void>}
 */
const testSave = async (body) => {
    try {
        const saveDailyData = new SaveDailyData(auth, S3);
        return await saveDailyData.save(body);
    } catch (err) {
        return err;
    }
};

const t0 = testSave(body);
const t1 = testSave({some:'bad', json:'object'});

Promise
    .all([t0, t1])
    .then(status => {
        let passed = true;
        // Test save endpoint
        if (!status[0]['ETag']) {
            console.log('t0 failed... ETag not returned');
            passed = false;
        }

        if (status[1]['message'] !== "Cipher data must be a string or a buffer") {
            console.log('t1 failed... Did not throw error on missing wasHappy parameter.');
            passed = false;
        }

        if (passed) {
            console.log("All tests passed.");
            process.exit(0);
        } else {
            console.log("Some tests failed.");
            process.exit(1);
        }
    });