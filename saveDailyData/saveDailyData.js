class SaveDailyData {
    /**
     * @param {Auth} auth
     * @param S3
     */
    constructor(auth, S3) {
        this.s3 = S3;
        this.decrypt = auth.decryptEncryptedRequest.bind(auth);
    }

    async save(data) {
        const wasHappy = this.decrypt(data['wasHappy']);
        const body     = Object.assign(data, wasHappy);

        const putSuccess = this.s3.putObject({
            Bucket: "happytracker",
            Key:  `${wasHappy['email']}/${data['date']}`,
            Body: JSON.stringify(body),
        }).promise();

        return await putSuccess;
    }

    /**
     * Returns the entire history of user tracked activity
     * @param {string} encryptedEmail
     * @returns {Promise<Array>}
     */
    async getAll(encryptedEmail) {
        const email = this.decrypt(encryptedEmail)['email'];
        const opts = {
            Bucket: 'happytracker',
            Prefix: email,
        };

        // Hit the S3.listObjectsV2 endpoint until it's done giving back keys
        let allKeys = [];
        do {
            const keys = await this.s3.listObjectsV2(opts).promise();

            for (let key of keys.Contents) {
                if (key['Key'].match(/\/20[0-9]{6}$/)) {
                    allKeys.push(key['Key']);
                }
            }

            opts.NextContinuationToken = keys.NextContinuationToken;
        } while (opts.NextContinuationToken);


        // Build a big ugly json object containing all of your data
        const jsonData = [];
        for (let key of allKeys) {
            const s3Object = await this.s3.getObject({Key: key, Bucket: 'happytracker'}).promise();
            jsonData.push(JSON.parse(s3Object.Body.toString()));
        }

        // Build the header row
        let header = [];
        for (let row of jsonData) {
            header = Array.from(new Set(header.concat(Object.keys(row))));
            // header = Object.assign(header, Object.keys(row));
        }

        // Create a key map of the header
        const headerMap = {};
        for (let [k, v] of Object.entries(header)) {
            headerMap[v] = k;
        }

        // Create an array of arrays mapping data to the header row
        const data = [header];
        for (let row of jsonData) {
            const rowAry = new Array(headerMap.length);
            for (let [col, val] of Object.entries(row)) {
                const key = headerMap[col];
                if (val === "on" || val === "yes") {
                    rowAry[key] = 1;
                } else if (val === "off" || val === "no") {
                    rowAry[key] = 0;
                } else {
                    rowAry[key] = val;
                }
            }
            data.push(rowAry);
        }

        return data;
    }
}

module.exports = SaveDailyData;